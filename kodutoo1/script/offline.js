console.log("OFFLINE JAVASCRIPT");
document.getElementById('body').innerHTML = '<div data-role="page" id="watchlist"><div data-role="header"><h1>WATCHLIST</h1></div><div data-role="main" class="ui-content" id="watchlistcontent"></div><div data-role="footer"><h1>Made by me as well!</h1></div></div><div data-role="page" id="detail"><div data-role="header"><a href="#watchlist" class="ui-btn ui-icon-arrow-l ui-btn-icon-notext" data-transition="slide"></a><h1>Detail View</h1></div><div data-role="main" class="ui-content" id="detailview"></div><div data-role="footer" class="ui-content"><h1>#GangstaShitMadeMeDoThis</h1></div></div><script src="script/offline.js"></script>';

function addNewMovie(id_name, movie) {
    var url = 'images/movie.png';
    var content = '<div style="overflow:hidden;" class="movie" index="' + movie.id + '" onclick="detailView(this);"><table><tr><td><img src="' + url + '" width="50" height="50" alt="movie.png"></td><td><ul><li><b>Name:</b>' + movie.title + '</li><li><b>Year:</b>' + movie.date + '</li><li><div id="myProgress"><div id="myBar" style="width: ' + movie.vote + '%;"><br></div></div></></ul></td></tr></table></div>';
    document.getElementById('' + id_name + '').innerHTML += content;
}

function toTheWatchList() {
    var moviesList = [];
    if (localStorage.getItem('items') != null) {
     moviesList = JSON.parse(localStorage['items']);   
    } else {
        moviesList.push("There are no movies!");
    }
    document.getElementById('watchlistcontent').innerHTML = "";
    for (var i = 0; i < moviesList.length; i++) {
        addNewMovie('watchlistcontent', moviesList[i]);
    }
    location.href='#watchlist';
}

function getMovie(id) {
    var storeList = [];
    storeList = JSON.parse(localStorage['items']);
    for (var i = 0; i < storeList.length; i++) {
        if (storeList[i].id == id) {
            return storeList[i];
        }
    }
}

function deleteFromStorage(el) {
    var fromStorage = [];
    var toStorage = [];
    fromStorage = JSON.parse(localStorage['items']);
    var movie = getMovie(el.getAttribute('index'));
    for (var i = 0; i < fromStorage.length; i++) {
        if (fromStorage[i].id != movie.id) {
            toStorage.push(fromStorage[i])
        }
    }   
    localStorage['items'] = JSON.stringify(toStorage);
    toTheWatchList();
}

function detailView(el) {
    var id = el.getAttribute('index');
    var movie = getMovie(id);
    
    var fun = "deleteFromStorage(this)";
    
    url = 'movie.png';
    var content = '<img src="' + url + '" width="250" height="250"><ul><hr><li><b>Title:</b></li><p>' + movie.title + '</p><li><b>Date:</b></li><p>' + movie.date + '</p><hr></ul><p><b>Overview: </b></p><p>' + movie.overview + '</p><button class="ui-btn ui-shadow ui-corner-all" id="watchbutton" style="background: #EF5350;" index="' + id + '" onclick="' + fun + ';">REMOVE</button>';
    document.getElementById('detailview').innerHTML = content;
    location.href = "#detail";
    
}

toTheWatchList();