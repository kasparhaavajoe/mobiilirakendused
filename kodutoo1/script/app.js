console.log("HEEEEEEEI");

var movies = [];
var num1 = 1;
var num2 = 1;

var apikey = '38af9ef4ab525c52f0cfa498bea4b747';
var value = "";

var imageBaseUrl = 'http://image.tmdb.org/t/p/w500';

function getTopMovies(page) {
    var url = 'http://api.themoviedb.org/3/movie/top_rated?api_key=' + apikey + '&language=en-US&page=' + page;
    $.get(url, function(data) {
        var results = data.results;
        var i;
        for (i = 0; i < results.length; i++) {
            var mov = results[i];
            var movie = {
                id: mov.id,
                title: mov.title,
                img_path: mov.poster_path,
                date: mov.release_date,
                overview: mov.overview,
                video: mov.video,
                vote: parseInt(mov.vote_average) * 10
            }
            movies.push(movie);
            console.log(movies.length);
            addNewMovie('movies',movie);
        }        
    })
    num1++;
}

function addNewMovie(id_name, movie) {
    var url = imageBaseUrl + movie.img_path;
    if (movie.img_path == null) {
        url = 'images/movie.png';
    }
    var content = '<div style="overflow:hidden;" class="movie" index="' + movie.id + '" onclick="detailView(this);"><table><tr><td><img src="' + url + '" width="50" height="50" alt="images/movie.png"></td><td><ul><li><b>Name:</b>' + movie.title + '</li><li><b>Year:</b>' + movie.date + '</li><li><div id="myProgress"><div id="myBar" style="width: ' + movie.vote + '%;"><br></div></div></></ul></td></tr></table></div>';
    document.getElementById('' + id_name + '').innerHTML += content;
}

document.getElementById('more').onclick = function() {
    getTopMovies(num1);
}

document.getElementById('moreResults').onclick = function() {
    getSearchedMovies(value, num2);
}

function toTheWatchList() {
    var moviesList = [];
    document.getElementById('watchlistcontent').innerHTML = "";
    var temp = localStorage.getItem('items');
    if (temp != null) {
     moviesList = JSON.parse(localStorage['items']);   
    } else {
        document.getElementById('watchlistcontent').innerHTML = '<h4>There are no movies currently</h4>';
    }
    if (moviesList.length == 0) {
        document.getElementById('watchlistcontent').innerHTML = '<h4>There are no movies currently</h4>';
    }
    for (var i = 0; i < moviesList.length; i++) {
        addNewMovie('watchlistcontent', moviesList[i]);
    }
    location.href = "#watchlist";
}

function searchMovies() {
    value = document.getElementById('searchtext').value;
    if (value.trim() != "") {
        console.log("hereereer");
        document.getElementById('results').innerHTML = "";
        getSearchedMovies(value.trim(), 1);
        location.href = "#searched";
    }
}

function getSearchedMovies(query, page) {
    var url = 'http://api.themoviedb.org/3/search/movie?api_key=' + apikey + '&language=en-US&query=' + query + '&page=' + page + '&include_adult=false';
    $.get(url, function(data) {
        var results = data.results;
        var i;
        for (i = 0; i < results.length; i++) {
            mov = results[i];
            var movie = {
                id: mov.id,
                title: mov.title,
                img_path: mov.poster_path,
                date: mov.release_date,
                overview: mov.overview,
                video: mov.video,
                vote: parseInt(mov.vote_average) * 10
            }
            movies.push(movie);
            console.log(movies.length);
            addNewMovie('results',movie);
        }        
    })
    num2++;
    
}

function detailView(el) {
    var id = el.getAttribute('index');
    console.log(id);
    var movie = getMovie(id);
    var storageList = [];
    
    if (localStorage.getItem('items') != null && localStorage['items'].length > 0) {
        storageList = JSON.parse(localStorage['items']);
    }
    var color = "#EF5350";
    var fun = "deleteFromStorage(this)";
    var buttext = "REMOVE";
    if (notInStorage(storageList, movie)) {
        color = "lightgreen";
        fun = "addToStorage(this)";
        buttext = "ADD";
    }
    
    result = [];    
     var url = 'http://api.themoviedb.org/3/movie/'+ parseInt(id) + '/credits?api_key=' + apikey + '';
    $.get(url)
        .done(function(data) {  
            results = data.cast;
            if (results.length >= 3) {
                result.push(results[0].name);
                result.push(results[1].name);
                result.push(results[2].name);
            } else {
                result.push("-");
            }
        
        url = imageBaseUrl + movie.img_path;
        if (movie.img_path == null) {
            url = 'images/movie.png';
        }
        console.log(result);
        var content = '<img src="' + url + '" width="250" height="250"><ul><hr><li><b>Title:</b></li><p>' + movie.title + '</p><li><b>Date:</b></li><p>' + movie.date + '</p><li><b>Actors:</b></li><p>'+ result + '</p><hr></ul><p><b>Overview: </b></p><p>' + movie.overview + '</p><button class="ui-btn ui-shadow ui-corner-all" id="watchbutton" style="background: ' + color + ';color:white;" index="' + id + '" onclick="' + fun + ';">' + buttext + '</button>';
        document.getElementById('detailview').innerHTML = content;
    })
    location.href = "#detail";
    console.log(result);
    
}

function getMovie(id) {
    for (var i = 0; i < movies.length; i++) {
        if (parseInt(movies[i].id) === parseInt(id)) {
            return movies[i];
        }
    }
    return -1;
}

function notInStorage(storageList, movie) {
    for (var i = 0; i < storageList.length; i++) {
        if (storageList[i].id === parseInt(movie.id)) {
            return false;
        }
    }
    return true;
}


function addToStorage(el) {
    var toStorage = [];
    if (localStorage.getItem('items') != null && localStorage['items'].length > 0) {
        toStorage = JSON.parse(localStorage['items']);
    }
    var movie = getMovie(el.getAttribute('index'));
    if (notInStorage(toStorage, movie)) {
        toStorage.push(movie);
        localStorage['items'] = JSON.stringify(toStorage);
    }
    console.log(localStorage);
    detailView(el);
}

function deleteFromStorage(el) {
    var fromStorage = [];
    var toStorage = [];
    if (localStorage['items'].length > 0) {
        fromStorage = JSON.parse(localStorage['items']);
        var movie = getMovie(el.getAttribute('index'));
        for (var i = 0; i < fromStorage.length; i++) {
            if (fromStorage[i].id != movie.id) {
                toStorage.push(fromStorage[i])
            }
        }
        localStorage['items'] = JSON.stringify(toStorage);
    }
    detailView(el);
}

getTopMovies(num1);